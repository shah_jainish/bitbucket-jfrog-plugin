# Atlassian Add-on using Express

Congratulations! You've successfully created an Atlassian Connect Add-on using the Express web application framework.

## What's next?

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).

<html>
  <head>
    <link rel="stylesheet" href="https://aui-cdn.atlassian.com/aui-adg/5.9.14/css/aui.min.css" media="all">
  </head>
  <body>
    <a class="aui-button aui-button-primary"
          href="https://bitbucket.org/site/addons/authorize?descriptor_uri=https://c926652d.ngrok.io&redirect_uri=https://c926652d.ngrok.io/icon.png">
       <span class="aui-icon aui-icon-small aui-iconfont-bitbucket"></span>
       Connect to Bitbucket
    </a>
  </body>
</html>